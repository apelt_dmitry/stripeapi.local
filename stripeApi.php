<?php
// secret key sk_test_Gv8w6TS9I6XBIlJJ00TuxpPD
// private key pk_test_jVv4uU2TMdqCeWdcgVga55Iu

if ($_POST['type'] == 'pay') {

  \Stripe\Stripe::setApiKey("sk_test_Gv8w6TS9I6XBIlJJ00TuxpPD");
  $error = '';
  $success = '';

  try {
    if (empty($_POST['street']) || empty($_POST['city']) || empty($_POST['zip']))
      throw new Exception("Fill out all required fields.");
    if (!isset($_POST['stripeToken']))
      throw new Exception("The Stripe Token was not generated correctly");
    $charge = \Stripe\Charge::create(array(
        "amount" => 1000,
        "currency" => "usd",
        "card" => $_POST['stripeToken'],
        "description" => $_POST['email'])
    );
    $success = '<div class=""><strong>Success!</strong> Payment was successful. </div>';


    try {
      $conn = new PDO('mysql:host=localhost;dbname=stripeapi', 'root', '');
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $stmt = $conn->prepare(
        'INSERT INTO payments (amount, id_charge, balance_transaction, created, currency, refunded)
        VALUES (:amount, :id_charge, :balance_transaction, :created, :currency, :refunded)'
      );
      $stmt->execute(array(
        ':amount' => $charge->amount,
        ':id_charge' => $charge->id,
        ':balance_transaction' => $charge->balance_transaction,
        ':created' => $charge->created,
        ':currency' => $charge->currency,
        ':refunded' => $charge->refunded
      ));
    } catch (PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
    }


  } catch (Exception $e) {
    $error = '<div class=""><strong>Error!</strong>' . $e->getMessage() . '</div>';
  }
} elseif ($_POST['type'] == 'refund') {
  \Stripe\Stripe::setApiKey("sk_test_Gv8w6TS9I6XBIlJJ00TuxpPD");
  $error = '';
  $success = '';

  try {
    $conn = new PDO('mysql:host=localhost;dbname=stripeapi', 'root', '');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = $conn->query('SELECT * FROM payments ORDER BY id DESC LIMIT 1');

  } catch (PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
  }

  $data_charge = null;

  foreach ($data as $row) {
    $data_charge = $row;
  }

  try {
    $re = \Stripe\Refund::create(array(
      "charge" => $data_charge['id_charge']
    ));
    $success = '<div class=""><strong>Success!</strong> Payment refunded. </div>';
  } catch (Exception $e) {
    $error = '<div class=""><strong>Error!</strong>' . $e->getMessage() . '</div>';
  }

  try {
    $conn = new PDO('mysql:host=localhost;dbname=stripeapi', 'root', '');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare('UPDATE payments SET refunded = :refunded WHERE id = :id');
    $stmt->execute(array(
      ':id' => $data_charge['id'],
      ':refunded' => true
    ));
  } catch (PDOException $e) {
    echo 'Error: ' . $e->getMessage();
  }


}