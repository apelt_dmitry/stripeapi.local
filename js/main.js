Stripe.setPublishableKey('pk_test_jVv4uU2TMdqCeWdcgVga55Iu');

function stripeResponseHandler(status, response) {
    if (response.error) {
        $('#error').css('display', 'block');
        $(".payment-errors").html(response.error.message);
    } else {
        validate = true;
        var form$ = $("#payment-form");
        var token = response['id'];
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        form$.get(0).submit();
    }
}
$(document).ready(function(){
    window.validate = false;
    $('#payment-form').submit(function () {
        if(!validate){
            submitHandler();
            return false;
        }
    });
});

function submitHandler() {
    console.log('handlers');
    Stripe.card.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val(),
        name: $('.card-holder-name').val(),
        address: $('.address').val(),
        address_city: $('.city').val(),
        address_zip: $('.zip').val(),
        address_state: $('.state').val(),
        address_country: $('.country').val()
    }, stripeResponseHandler);
    return false;
}

