<?php
require 'stripe-php/init.php';
require 'stripeApi.php';
?>

<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="UTF-8">
  <script src="js/jquery-1.11.3.min.js"></script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  <script src="js/main.js"></script>
  <title>Stripe API Test Page</title>
</head>
<body>
<form action="" method="POST" id="payment-form" class="">
  <input type="hidden" name="type" value="pay">
  <div class="alert alert-danger" id="error" style="display: none;">
    <strong>Something wrong!!!</strong>
    <span class="payment-errors"></span>
  </div>

  <span class="payment-success">
  <?= $success ?>
  <?= $error ?>
  </span>

    <h2>Client Info</h2>

    <div class="form-group">
      <label class="" for="textinput">Street</label>

      <input type="text" name="street" placeholder="Street" class="address form-control">
    </div>

    <div class="form-group">
      <label class="" for="textinput">City</label>
      <input type="text" name="city" placeholder="City" class="city form-control">
    </div>

    <div class="form-group">
      <label class="" for="textinput">State</label>
      <input type="text" name="state" maxlength="65" placeholder="State" class="state form-control">
    </div>

    <div class="form-group">
      <label class="" for="textinput">Postal Code</label>
      <input type="text" name="zip" maxlength="9" placeholder="Postal Code" class="zip form-control">
    </div>

    <div class="form-group">
      <label class="" for="textinput">Country</label>
      <input type="text" name="country" placeholder="Country" class="country form-control">
    </div>

    <div class="form-group">
      <label class="" for="textinput">Email</label>
      <input type="text" name="email" maxlength="65" placeholder="Email" class="email form-control">
    </div>
    <h2>Card Details</h2>

    <div class="form-group">
      <label class="" for="textinput">Card Holder's Name</label>
      <input type="text" name="cardholdername" maxlength="32" placeholder="Card Holder Name" class="card-holder-name form-control">
    </div>

    <div class="form-group">
      <label class="" for="textinput">Card Number</label>
      <input type="text" id="cardnumber" maxlength="19" placeholder="Card Number" class="card-number form-control">
    </div>

    <div class="form-group">
      <label class="" for="textinput">Card Expiry Month</label>
      <input type="text" id="exp-month" maxlength="2" placeholder="12" class="card-expiry-month form-control">
    </div>

    <div class="form-group">
      <label class="" for="textinput">Card Expiry Year</label>
      <input type="text" id="exp-year" maxlength="4" placeholder="2017" class="card-expiry-year form-control">
    </div>

    <div class="form-group">
      <label class="" for="textinput">CVV</label>
      <input type="text" id="cvv" placeholder="CVV" maxlength="4" class="card-cvc form-control">
    </div>
    <button class="btn" type="submit">Pay Now</button>
</form>
<?php
if($success && !$data_charge){
  ?>
<form action="" method="POST" id="payment-form" class="">
  <input type="hidden" name="type" value="refund">
  <button class="btn" type="submit">Refund</button>
</form>
<?php
}
?>


</body>
</html>